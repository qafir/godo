/*
 * Copyright (c) 2016, Alfredo Mungo
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef _LOGIN_H
#define _LOGIN_H

#include <stdbool.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>

/*
  Return a passwd structure for the given user.

  ARGUMENTS
    user: A null-terminated string representing the user name or numeric ID

  RETURN VALUE
    Pointer to a static passwd structure or NULL on failure
*/
struct passwd *get_user(const char *user);

/*
  Return a group structure for the given group.

  ARGUMENTS
    group: A null-terminated string representing the group name or numeric ID

  RETURN VALUE
    Pointer to a static group structure or NULL on failure
*/
struct group *get_group(const char *group);

/*
  Return the current user's shell.

  RETURN VALUE
    Pointer to a static string containing the shell path for the current user
*/
const char *get_shell();

/*
  Check identifier validity.

  ARGUMENTS
    ident: A null-terminated string containing the user or group name

  RETURN VALUE
    True if the given identity is valid, false if not
*/
bool is_login_valid(const char *ident);

/*
  Authenticate current user.

  ARGUMENTS
    username: The user name or string representation of user ID

  RETURN VALUE
    True if the user has successfully been authenticated, false if not
*/
bool authenticate();

/*
  Tell whether a user is a member of a group.

  ARGUMENTS
    user: The user
    group: The group represented as a name or number

  RETURN VALUE
    True if the user is a member of a given group, false if not
*/
bool is_user_in_group(const char *user, const char *group);

/*
  Check whether the current user login status persists or not.

  RETURN VALUE
    True if the login persists, false if not.
*/
bool login_persists();

/*
  Create/Update login persistence file.
*/
void login_set_persistent();

#endif /* _LOGIN_H */
