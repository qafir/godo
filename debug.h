/*
 * Copyright (c) 2016, Alfredo Mungo
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef _DEBUG_H
#define _DEBUG_H

/*
  This header file contains error reporting routines for debugging.

  The following public macros are defined:
    dbgerr(msg): Print an error message
    dbgerr2(fmt, ...): Print a printf-style message
    dbgerrno: Expands to a dbgerr2() call printing a text description of errno

    dbgnotice(msg), dbgnotice2(fmt, ...): Same as their -err counterparts but
      for notices

    The following internal-use-only macros are defined:
      dbgprint(mtype, msg): Prints a message
      dbgprint2(mtype, fmt, ...): Prints a printf-style message
*/

#include <stdio.h>
#include <string.h>
#include <errno.h>

/* Constants */
#define DBG_ERROR "ERROR"
#define DBG_NOTICE "NOTICE"

/* Error */
#define dbgerr(msg) dbgprint(DBG_ERROR, msg)
#define dbgerr2(fmt, ...) dbgprint2(DBG_ERROR, fmt, __VA_ARGS__)
#define dbgerrno dbgerr2("%s\n", strerror(errno))

#ifndef NDEBUG
  /* Generic */
  #define dbgprint(mtype, msg) fprintf(stderr, "%s:%d :: " mtype ": %s", __FILE__, __LINE__, msg)
  #define dbgprint2(mtype, fmt, ...) fprintf(stderr, "%s:%d :: " mtype ": " fmt, __FILE__, __LINE__, __VA_ARGS__)

  /* Notice */
  #define dbgnotice(msg) dbgprint(DBG_NOTICE, msg)
  #define dbgnotice2(fmt, ...) dbgprint2(DBG_NOTICE, fmt, __VA_ARGS__)
#else /* NDEBUG */
  /* Generic */
  #define dbgprint(mtype, msg) fprintf(stderr, mtype ": %s", msg)
  #define dbgprint2(mtype, fmt, ...) fprintf(stderr, mtype ": " fmt, __VA_ARGS__)

  /* Notice */
  #define dbgnotice(msg)
  #define dbgnotice2(fmt, ...)
#endif

#endif /* _DEBUG_H */
