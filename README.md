# NAME

godo - execute a command as another user or group

# SYNOPSIS

godo \[OPTIONS\] \[--\] **{COMMAND}**

# DESCRIPTION

godo executes a command as a different local user based on a system-wide, per-user/group policy. The application first checks the ownership and permissions of its configuration file; if valid, it parses the configuration file, reporting and exiting on errors. After a successfull configuration file read, the last rule matching the command line provided request (if any) is enforced and the given command executed.

To authenticate the user PAM is used.

# OPTIONS

- -C **{CONFIG\_FILE\_PATH}**

    Run in parse-only mode. In this mode, the program exits immediately after parsing the given configuration file.

- -s

    Run in shell mode. In this mode, the program runs the current user's shell as the target user.

- -u **{IDENTITY}**

    Set the target identity. IDENTITY can be supplied as a username and/or group (see EXAMPLE). If this option is not provided, the command is intended to be run as "root:root".

- -h

    Print the help message and exit.

- -- 

    After the `--` switch, the remaining command line arguments are all treated as part of the command to run. Use this if you want to provide switches to the command you want to run.

# CONFIGURATION

The configuration file, found at `/etc/godo.conf`, contains the execution policy as a list of rules. Each rule terminates with a newline character, comment lines begin with a `#` character and empty lines are allowed. Each rule must begin with one of _permit_, _deny_ to permit or deny the given command/target/subject combination. The second field is optional and its contents must be put between brackets (`(` and `)`); it consists of a list of options (See **Options** below). The third field is the _subject_: a subject is the identity (see **Identity** below) to which the rule applies. The fourth field is optional and must be preceeded by the `as` keyword and represents the identity which the subject is permitted/denied to execute the command as (defaults to `root:root`). The fifth argument, if present, is the command which is permitted/denied and must be preceeded by the keyword `cmd` (defaults to all commands). The sixth argument is optional and can be provided only if a command is given to the rule, must be preceeded by the `args` keyword and states the exact arguments and order of arguments which the command must have on the command line to be permitted/denied; if a rule displays the `args` keyword but no arguments, the rule is valid for the command with no arguments (defaults to any argument in any order).

Such a rule is resumed here:

- permit/deny \['(' OPTION\_LIST ')'\] SUBJECT \[as TARGET\] \[cmd COMMAND \[args \[ARGUMENT LIST\]\]\]

## Options

The following options can be used in rules:

- nopass

    Don't ask for a password

- persist

    Persistent login. Don't ask for a password for some time (defined at compile time, defaults to 10 minutes).

- keepenv

    Keep the default environment variables.

- setenv { VARIABLE1="CONTENT 1" VARIABLE2=CONTENT\_2 VARIABLE3= VARIABLE4 ... }

    Set the variables listed between brackets. Variables are space separated and variable names are separated by their values by an equal sign (`=`). Variables which contain spaces can be set by enclosing their value between double quotes (`"`). If the _keepenv_ option is used, the form `VARIABLE=` (with no value) can be used to tell godo not to keep that particular variable, while if the option is not provided, `VARIABLE` (with no equal sign and value) can be used to keep that specific one.

## Identity

An identity is a user name, a group name of a combination of those. Identities are provided in the form `user:group` where `user` and `group` are optional and at least one of the two must be present. Valid identity examples are:

- root:root
- myusername:users
- mysql:daemons
- myusername
- :mygroupname

# EXIT STATUS

On success, this program does not return. Refer to the manual of the program you are trying to run. On failure, a non-zero value is returned.

# FILES

- `/etc/godo.conf`

    godo configuration file

# BUGS

Submit bug reports from [https://gitlab.com/qafir/godo/issues](https://gitlab.com/qafir/godo/issues) or send'em by email to <alfredo.mungo@openmailbox.org>. Be sure to include your configuration file, your PAM configuration and the command line from which the issue arises.

# EXAMPLE

- godo echo hello, world

    Run the command as the _root_ user and the _root_ group.

- godo -u :network -- netrun -t home

    Run the command `netrun -t home` as the _root_ user and _network_ group.

- godo -u foo:bar -- ls -l

    Run the command `ls -l` as the _foo_ user and _bar_ group.

- godo -u mysql -s

    Run your preferred shell (as set in `/etc/passwd`) as the _mysql_ user and _root_ group.

# AUTHORS

Written by Alfredo Mungo <alfredo.mungo@openmailbox.org>.

# COPYRIGHT

Copyright © 2016 Alfredo Mungo. License BSD 3-Clause [https://opensource.org/licenses/BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause).

# SEE ALSO

**su**(1), **pam**(8)
