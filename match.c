/*
 * Copyright (c) 2016, Alfredo Mungo
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include <grp.h>
#include "parser-utils.h"
#include "ptrpool.h"
#include "login.h"
#include "config.h"
#include "debug.h"

/*
  Match functions.
  Every given rule pointer which does not match is set NULL.

  ARGUMENTS
    rules: Array of rules to check for match
    nrules: Length of `rules`
*/
static void match_subject(struct RULE **rules, size_t nrules);
static void match_command(struct RULE **rules, size_t nrules);
static void match_target(struct RULE **rules, size_t nrules);

struct RULE **match() {
  struct RULE **r = get_rules(), **r2;
  size_t n = get_nrules();
  size_t nmatching = 0;

  match_command(r, n);
  match_subject(r, n);
  match_target(r, n);

  for(size_t i = 0; i < n; i++)
    if(r[i])
      nmatching++;

  r2 = calloc(nmatching + 1, sizeof(struct RULE *));
  
  for(size_t i = 0, nmatching = 0; i < n; i++)
    if(r[i])
      r2[nmatching++] = r[i];

  free(r);
  return r2;
}

static void match_subject(struct RULE **rules, size_t nrules) {
  struct passwd *p = getpwuid(getuid());

  dbgnotice2("Subject: uid=%llu pwnam=%s\n", (unsigned long long)p->pw_uid, p->pw_name);

  for(size_t i = 0; i < nrules; i++)
    if(rules[i]) {
      struct RULE *r = rules[i];

      #ifndef NDEBUG
      {
        const char *user = r->subject.username;
        const char *group = r->subject.group;

        dbgnotice2("\tChecking match with: user=%s group=%s... ", user? user: "(none)", group? group: "(none)");
      }
    #endif

      if(r != NULL) {
        if(r->subject.username) { /* Username is not null */
          size_t len = strlen(r->subject.username);
          bool numeric = true;

          for(size_t i = 0; i < len; i++)
            if(!isdigit(r->subject.username[i])) {
              numeric = false;
              break;
            }

          if(!numeric) { /* Username given */
            if(strcmp(r->subject.username, p->pw_name))
              rules[i] = NULL;
          } else { /* UID given */
            uid_t cuid = (uid_t)strtoul(r->subject.username, NULL, 10);

            if(cuid != p->pw_uid)
              rules[i] = NULL;
          }
        }

        if(r->subject.group && !is_user_in_group(p->pw_name, r->subject.group))
          rules[i] = NULL;

        #ifndef NDEBUG
          fprintf(stderr, "%s\n", rules[i]? "MATCH": "NO MATCH");
        #endif
      }
    }
}

static void match_command(struct RULE **rules, size_t nrules) {
  #ifndef NDEBUG
    dbgnotice("Command:");

    for(size_t i = 0; i < cl_command_len; i++)
      fprintf(stderr, " %s", cl_command[i]);

    fprintf(stderr, "\n");
  #endif

  for(size_t i = 0; i < nrules; i++)
    if(rules[i]) {
      struct RULE *r = rules[i];

      if(r->cmd) {
        #ifndef NDEBUG
          dbgnotice2("\tChecking match with: %s", r->cmd);

          if(r->has_args)
            for(size_t j = 0; r->args[j]; j++)
              fprintf(stderr, " %s", r->args[j]);

          fprintf(stderr, "... ");
        #endif

        if(strcmp(r->cmd, *cl_command)) {
          rules[i] = NULL;

          #ifndef NDEBUG
            fprintf(stderr, "NO MATCH\n");
          #endif
        } else { /* Command matches, check arguments */
          if(r->has_args) {
            size_t nargs;

            for(nargs = 0; r->args[nargs]; nargs++);

            if(nargs == cl_command_len - 1) { /* Number of arguments is good */
              for(size_t j = 1; j < cl_command_len && r->args[j-1]; j++)
                if(strcmp(cl_command[j], r->args[j-1])) {
                  rules[i] = NULL;

                  #ifndef NDEBUG
                    fprintf(stderr, "NO MATCH\n");
                  #endif
                }

              #ifndef NDEBUG
                if(rules[i])
                  fprintf(stderr, "MATCH\n");
              #endif
            } else { /* Wrong number of arguments */
              rules[i] = NULL;

              #ifndef NDEBUG
                fprintf(stderr, "NO MATCH\n");
              #endif
            }
          } else {
            #ifndef NDEBUG
              fprintf(stderr, "MATCH\n");
            #endif
          }
        }
      }
    }
}

static void match_target(struct RULE **rules, size_t nrules) {
  dbgnotice2("Target: %s:%s\n", cl_username, cl_group);
  for(size_t i = 0; i < nrules; i++) {
    if(rules[i]) {
      struct RULE *r = rules[i];

      dbgnotice2("\tChecking match with: user=%s group=%s...", r->target.username? r->target.username: "(none)", r->target.group? r->target.group: "(none)");

      if(r->target.username) { /* Rule defines a user */
        struct passwd *u = get_user(cl_username);

        if(u) {
          if(strcmp(u->pw_name, r->target.username)) {
            rules[i] = NULL;
            #ifndef NDEBUG
              fprintf(stderr, " NO MATCH\n");
            #endif
            continue;
          }
        } else { /* User in rule doesn't exist */
          rules[i] = NULL;
            #ifndef NDEBUG
              fprintf(stderr, " NO MATCH\n");
            #endif
          continue;
        }
      }

      if(r->target.group) { /* Rule defines a group */
        struct group *g = get_group(cl_group);

        if(g) {
          if(strcmp(g->gr_name, r->target.group)) {
            rules[i] = NULL;
            #ifndef NDEBUG
              fprintf(stderr, " NO MATCH\n");
            #endif
            continue;
          }
        } else { /* Group in rule doesn't exist */
          rules[i] = NULL;
          #ifndef NDEBUG
            fprintf(stderr, " NO MATCH\n");
          #endif
          continue;
        }
      }

      #ifndef NDEBUG
        fprintf(stderr, " MATCH\n");
      #endif
    }
  }
}
