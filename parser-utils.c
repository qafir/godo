/*
 * Copyright (c) 2016, Alfredo Mungo
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
*/
#define _PARSER_UTILS_C

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include "parser-utils.h"
#include "rule.h"
#include "ptrpool.h"
#include "debug.h"

struct PTRPOOL rule_pool, env_pool, arg_pool;

/*
  (Re)set string with.

  ARGUMENTS
    to: Pointer to a heap-allocated string (or NULL) to reset
    from: Pointer to a string holding the value that will be copied to `*to`
*/
static void set_string(char **to, const char *from);

/*
  Set rule identity from string.

  ARGUMENTS
    id: The identity structure to set
    s: A string representing the identity to parse and set
*/
static void set_ident(struct RULE_IDENTITY *id, const char *s);

/*
  Reset the buffer pools.
*/
static void reset_pools();

/*
  Clean a rule by deallocating its contents.
*/
static void clean_rule(void *ptr);

void init() {
  buff_str = NULL;

  memset(&cur_rule, 0, sizeof(struct RULE));
  memset(&cur_var, 0, sizeof(struct RULE_VARIABLE));

  pp_init(&rule_pool, 12, 12);
  pp_init(&env_pool, 6, 6);
  pp_init(&arg_pool, 6, 6);
}

struct RULE **get_rules() {
  struct RULE **rules = malloc(sizeof(struct RULE *) * rule_pool.sz);

  for(size_t i = 0; i < rule_pool.sz; i++)
    rules[i] = rule_pool.pool[i];

  return rules;
}

size_t get_nrules() {
  return rule_pool.sz;
}

static void reset_pools() { /* Don't reset the rule pool. Trust me. */
  pp_clean(&env_pool);
  pp_clean(&arg_pool);
}

void cleanup() {
  pp_cleanall(&rule_pool, clean_rule);
  reset_pools();
  free(buff_str);
}

static void set_string(char **to, const char *from) {
  if(from) {
    size_t len = strlen(from) + 1;

    *to = realloc(*to, sizeof(char) * len);
    strcpy(*to, from);
  } else {
    free(*to);
    *to = NULL;
  }
}

void set_subject(const char *s) {
  set_ident(&cur_rule.subject, s);
}

void set_target(const char *s) {
  set_ident(&cur_rule.target, s);
}

static void set_ident(struct RULE_IDENTITY *id, const char *s) {
  size_t slen = strlen(s);
  char *str = malloc(sizeof(char) * (slen + 1));
  char *tok, *ptr;

  strcpy(str, s);

  if(str[0] != ':') { /* Username & possibly group */
    tok = strtok_r(str, ":", &ptr);
    set_string(&id->username, tok);

    tok = strtok_r(NULL, "", &ptr);
    set_string(&id->group, tok);
  } else { /* Group only */
    set_string(&id->username, NULL);
    set_string(&id->group, (s + 1));
  }

  free(str);
}

void set_command(const char *s) {
  set_string(&cur_rule.cmd, s);
}

void set_cur_var_name(const char *s) {
  set_string(&cur_var.name, s);
}

void set_cur_var_value(const char *s) {
  set_string(&cur_var.value, s);
}

void add_env() {
  struct RULE_VARIABLE *v = malloc(sizeof(struct RULE_VARIABLE));

  v->name = cur_var.name;
  v->value = cur_var.value;

  if(!v->value) /* Variable has no value, try getting it from the environment */
    set_string(&v->value, getenv(v->name));

  if(v->value)
    pp_add(&env_pool, v);
  else { /* Variable has no value, no point in keeping it */
    free(cur_var.name);
    free(v);
  }

  cur_var.name = NULL;
  cur_var.value = NULL;
}

void add_arg() {
  pp_add(&arg_pool, buff_str);

  buff_str = NULL;
}

void set_string_buffer(const char *s) {
  if(s) {
    size_t slen = strlen(s) + 1;

    buff_str = realloc(buff_str, sizeof(char) * slen);
    strcpy(buff_str, s);
  } else {
    free(buff_str);
    buff_str = NULL;
  }
}

void add_rule() {
  size_t i;
  struct RULE *new_rule = malloc(sizeof(struct RULE));

  if(cur_rule.has_args) { /* Add arguments if needed */
    cur_rule.args = malloc(sizeof(char *) * (arg_pool.sz + 1));
    
    for(i = 0; i < arg_pool.sz; i++)
      cur_rule.args[i] = arg_pool.pool[i];

    cur_rule.args[arg_pool.sz] = NULL;
  }

  if(cur_rule.options & RULEOPT_SETENV) { /* Add environment variables if needed */
    cur_rule.env = malloc(sizeof(struct RULE_VARIABLE *) * (env_pool.sz + 1));

    for(i = 0; i < env_pool.sz; i++)
      cur_rule.env[i] = env_pool.pool[i];

    cur_rule.env[env_pool.sz] = NULL;
  }

  dbgnotice("Adding rule...\n");

  memcpy(new_rule, &cur_rule, sizeof(struct RULE));
  pp_add(&rule_pool, new_rule);
  memset(&cur_rule, 0, sizeof(struct RULE));
  reset_pools();
}

static void clean_rule(void *ptr) {
  struct RULE *r = ptr;

  if(r->env) {
    for(struct RULE_VARIABLE **p = r->env; *p != NULL; p++) {
      free((*p)->name);
      free((*p)->value);

      free(*p);
    }

    free(r->env);
  }

  if(r->args) {
    for(char **p = r->args; *p != NULL; p++)
      free(*p);

    free(r->args);
  }

  if(r->cmd)
    free(r->cmd);

  if(r->subject.username)
    free(r->subject.username);

  if(r->subject.group)
    free(r->subject.group);

  if(r->target.username)
    free(r->target.username);

  if(r->target.group)
    free(r->target.group);
}
