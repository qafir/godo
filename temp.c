#include <stdlib.h>
#include <string.h>
#include "config.h"

char *get_temp_dir() {
  const char *vars[] = {
    "TMPDIR",
    "TMP",
    "TEMP",
    "TEMPDIR",
    NULL
  }; /* Environment variables to check for temporary files directory */
  const char *tmpdir;
  char *out;
  size_t out_len;

  for(const char **v = vars; *v; v++)
    if((tmpdir = getenv(*v)))
      break;

  if(!tmpdir)
    tmpdir = DEFAULT_TEMPDIR;

  out = strdup(tmpdir);
  out_len = strlen(out);

  /* Trim trailing '/' */
  while(out_len > 0 && out[out_len - 1] == '/')
    out[--out_len] = '\0';

  return out;
}
