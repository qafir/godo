/*
 * Copyright (c) 2016, Alfredo Mungo
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef _PARSER_UTILS_H
#define _PARSER_UTILS_H

#include "rule.h"

#ifdef _PARSER_UTILS_C
  #define EXTERN extern
#else
  #define EXTERN
#endif

/*
  Set the PERMIT flag to the current rule buffer.

  ARGUMENTS
    b_permit: Boolean value representing whether the rule permits or denies
      its command
*/
#define set_permit(b_permit) do { cur_rule.permit = (b_permit)? 1: 0; } while(0)

/*
  Enables the given rule option in the current rule buffer.

  ARGUMENTS
    u_upt: An unsigned integer representing a possibly ORed list of options to
      enable
*/
#define set_option(u_opt) do { cur_rule.options |= (u_opt); } while(0)

EXTERN struct RULE cur_rule; /* Current rule buffer */
EXTERN struct RULE_VARIABLE cur_var; /* Current environment variable buffer */
EXTERN char *buff_str; /* Generic string buffer */

/*
  Perform parser utils initialisation.
*/
void init();

/*
  Perform parser utils cleanup.
*/
void cleanup();

/*
  Set current rule subject.

  ARGUMENTS
    s: String representing the subject identity
*/
void set_subject(const char *s);

/*
  Set current rule target.

  ARGUMENTS
    s: String representing the taret identity
*/
void set_target(const char *s);

/*
  Set current rule command.

  ARGUMENTS
    s: String representing the command to be executed (w/o arguments)
*/
void set_command(const char *s);

/*
  Set current environment variable name.

  ARGUMENTS
    s: Name
*/
void set_cur_var_name(const char *s);

/*
  Set current environment variable value.

  ARGUMENTS
    s: Value
*/
void set_cur_var_value(const char *s);

/*
  Add the environment variable buffer data to the current rule buffer.
*/
void add_env();

/*
  Add the current argument contained in the generic string buffer to the
  arguments list.
*/
void add_arg();

/*
  Add the data contained in the current rule buffer to the list of parsed
  rules.
*/
void add_rule();

/*
  Set the content of the generic string buffer.
*/
void set_string_buffer(const char *s);

/*
  Get number of parsed rules.

  RETURN VALUE
    The number of currently parsed rules
*/
size_t get_nrules();

/*
  Get parsed rules.

  RETURN VALUE
    An array containing the currently parsed rules. The returned pointer must
    be manually freed by the calling routine. This function always returns THE
    VERY SAME data inside a new array.
*/
struct RULE **get_rules();

#undef EXTERN

#endif /* _PARSER_UTILS_H */
