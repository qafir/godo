/*
 * Copyright (c) 2016, Alfredo Mungo
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "debug.h"
#include "env.h"

#define NAME_BUFF_LEN 512 /* Length of environment variable name buffer */

void setenv2(char **env, const char *name, const char *value) {
  char **ptr = NULL;
  char buff[NAME_BUFF_LEN];
  size_t content_len = strlen(name) + (value? strlen(value): 0) + 2 /* '=' + '\0' */;
  size_t i;

  /* Check if already set */
  for(i = 0; env[i]; i++) {
    if(!getenv_name(env[i], buff, NAME_BUFF_LEN))
      exit(EXIT_FAILURE);

    if(strcmp(buff, name) == 0) { /* Already set */
      ptr = &env[i];
      break;
    }
  }

  if(!ptr)
    ptr = &env[i];

  *ptr = realloc(*ptr, value? sizeof(char) * content_len: 0);

  if(*ptr)
    sprintf(*ptr, "%s=%s", name, value);
}

bool getenv_name(const char *envstr, char *outname, size_t outname_len) {
  char *envstr2 = strdup(envstr);
  char *tokout, *tokstat;
  bool retval = false;

  tokout = strtok_r(envstr2, "=", &tokstat);
  outname[outname_len - 1] = '\0';

  if(tokout) { /* Non-empty string or name */
    strncpy(outname, tokout, outname_len);

    if(outname[outname_len - 1] != '\0') /* Truncation occurred */
      outname[outname_len - 1] = '\0';
    else
      retval = true;
  } else
    outname[0] = '\0';

  free(envstr2);
  return retval;
}

char **getenv_content_ptr_from_array(char **env, const char *name) {
  char buff[NAME_BUFF_LEN];

  for(size_t i = 0; env[i]; i++) {
    if(!getenv_name(env[i], buff, NAME_BUFF_LEN)) {
      dbgerr2("Environment variables name lenght is currently limited to %u characters\n", NAME_BUFF_LEN);
      exit(EXIT_FAILURE);
    }

    if(strcmp(buff, name) == 0)
      return &env[i];
  }

  return NULL;
}

char **getenv_all_names(char **env) {
  size_t envn;
  char **out;
  char buff[NAME_BUFF_LEN];

  for(envn = 0; env[envn]; envn++);

  out = calloc(envn + 1, sizeof(char *));
  
  for(envn = 0; env[envn]; envn++) {
    if(getenv_name(env[envn], buff, NAME_BUFF_LEN)) {
      out[envn] = strdup(buff);
    } else {
      dbgerr2("Environment variables name lenght is currently limited to %u characters\n", NAME_BUFF_LEN);
      exit(EXIT_FAILURE);
    }
  }

  return out;
}
