#!/bin/bash

perl -pe "s/\@\@APP_NAME\@\@/${BINNAME}/g;" \
	-e "s/\@\@APP_VERSION_MAJOR\@\@/${APP_VERSION_MAJOR}/g;" \
	-e "s/\@\@APP_VERSION_MINOR\@\@/${APP_VERSION_MINOR}/g;" \
	-e "s/\@\@APP_VERSION_PATCH\@\@/${APP_VERSION_PATCH}/g;" \
	-e "s/\@\@APP_VERSION_PHASE\@\@/${APP_VERSION_PHASE}/g;" \
	-e "s/\@\@BUILD_UUID\@\@/${BUILD_UUID}/g;" \
	-e "s#\@\@CONFIG_PATH\@\@#${CONFIG_PATH}#g;" \
	-e "s#\@\@BINDIR\@\@#${BINDIR}#g;" \
	-e "s#\@\@ETCDIR\@\@#${ETCDIR}#g;" \
	-e "s#\@\@SHAREDIR\@\@#${SHAREDIR}#g;" \
	-e "s#\@\@PERSIST_INTERVAL\@\@#${PERSIST_INTERVAL}#g;"
