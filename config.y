/*
 * Copyright (c) 2016, Alfredo Mungo
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
*/
%start ruleset

%left PERMIT DENY COMMENT

%token COMMA
%token EQ
%token BR_OPEN BR_CLOSE BLOCK_OPEN BLOCK_CLOSE
%token STRING_DELIM
%token STRING VARNAME VARNAME_NOCONTENT
%token AS COMMAND CMD ARGS
%token KEEPENV SETENV LOGIN NOPASS PERSIST

%right EOR

%{
#include <stdio.h>
#include "parser-utils.h"
extern char yytext[];

extern void yyerror(const char *s);
extern int yylex();

#ifndef NDEBUG
extern int yydebug;
#endif
%}

%initial-action { init(); }

%%

ruleset: rule
| rule ruleset
| notrule ruleset
| notrule
;

notrule: COMMENT
| COMMENT notrule
| EOR
| EOR notrule
;

rule: permission option_block auth EOR { add_rule(); }
| permission auth EOR { add_rule(); }
| permission option_block auth command EOR { add_rule(); }
| permission auth command EOR { add_rule(); }
;

command: cmd
| cmd args
;

cmd: CMD COMMAND { set_command(yytext); }
;

args: ARGS argument_list
| ARGS
;

argument_list: argument argument_list
| argument
;

argument: text { add_arg(buff_str); }

auth: login
| login auth_target
;

login: LOGIN { set_subject(yytext); }
;

auth_target: AS LOGIN { set_target(yytext); }
;

permission: PERMIT { set_permit(1); }
| DENY { set_permit(0); }
;

option_block: BR_OPEN option_list BR_CLOSE;

option_list: option COMMA option_list
| option
;

option: KEEPENV { set_option(RULEOPT_KEEPENV); }
| PERSIST { set_option(RULEOPT_PERSIST); }
| NOPASS { set_option(RULEOPT_NOPASS); }
| setenv_block { set_option(RULEOPT_SETENV); }
;

setenv_block: SETENV BLOCK_OPEN env_list BLOCK_CLOSE;

env_list: env_var env_list
| env_var
;

env_var: varname EQ text { set_cur_var_value(buff_str? buff_str: ""); add_env(); }
| varname { add_env(); }
;

varname: VARNAME { set_cur_var_name(yytext); };

text: STRING_DELIM string STRING_DELIM
| STRING_DELIM STRING_DELIM { set_string_buffer(NULL); }
| string
;

string: STRING { set_string_buffer(yytext); }
