/*
 * Copyright (c) 2016, Alfredo Mungo
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
*/
#include <stdlib.h>
#include "ptrpool.h"

void pp_init(struct PTRPOOL *p, size_t initial_cap, size_t cap_incr) {
  p->cap_initial = initial_cap;
  p->cap_incr = cap_incr;
  p->pool = NULL;
  p->sz = 0;
}

void pp_clean(struct PTRPOOL *p) {
  free(p->pool);

  p->sz = 0;
  p->pool = NULL;
}

void pp_cleanall(struct PTRPOOL* p, PP_CLEANFUNC f) {
  for(size_t i = 0; i < p->sz; i++) {
    if(f)
      f(p->pool[i]);

    free(p->pool[i]);
  }

  pp_clean(p);
}

void pp_grow_if_needed(struct PTRPOOL *p) {
  if(!p->pool || p->sz == p->cap) {
    /* Needs to grow */
    p->cap += p->cap_incr;

    p->pool = realloc(p->pool, sizeof(void *) * p->cap);
  }
}

void pp_add(struct PTRPOOL *pool, void *ptr) {
  pp_grow_if_needed(pool);

  pool->pool[pool->sz++] = ptr;
}
