/*
 * Copyright (c) 2016, Alfredo Mungo
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * 
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
 * WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
*/
#include <limits.h>
#include <stdbool.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <termios.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include <ctype.h>
#include <time.h>
#include <security/pam_appl.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "login.h"
#include "config.h"
#include "temp.h"
#include "debug.h"

/*
  PAM conversation function.
  Refer to the manual page `pam_conv(3)`.
*/
static int pam_conv(int num_msg, const struct pam_message **msg, struct pam_response **resp, void *appdata_ptr);

/*
  Return the persistance file path for the current user.

  RETURN VALUE
    A string containing the path of the persistance file for the given user.
    The calling routine must take care of freeing the returned memory.
*/
static char *get_persistance_file_path();

struct passwd *get_user(const char *user) {
  size_t len = strlen(user);
  bool numeric = true;

  for(size_t i = 0; i < len; i++)
    if(!isdigit(user[i])) {
      numeric = false;
      break;
    }

  if(numeric)
    return getpwuid((uid_t)strtoul(user, NULL, 10));
  else
    return getpwnam(user);
}

struct group *get_group(const char *group) {
  size_t len = strlen(group);
  bool numeric = true;

  for(size_t i = 0; i < len; i++)
    if(!isdigit(group[i])) {
      numeric = false;
      break;
    }

  if(numeric)
    return getgrgid((uid_t)strtoul(group, NULL, 10));
  else
    return getgrnam(group);
}

const char *get_shell() {
 return getpwuid(getuid())->pw_shell;
}

bool is_login_valid(const char *ident) {
  if(!isalpha(ident[0]))
    return false;

  for(size_t i = 1; ident[i]; i++)
    if(!isalnum(ident[i]))
      return false;

  return true;
}

bool authenticate() {
  #define CID_LEN 12
  char current_user_id[CID_LEN];
  snprintf(current_user_id, CID_LEN, "%lu", (unsigned long)getuid());
  #undef CID_LEN

  struct passwd *u = get_user(current_user_id);
  struct pam_conv conv = {
    .conv = pam_conv,
    .appdata_ptr = NULL
  };
  pam_handle_t *hnd = NULL;
  int pam_error = 0;

  if(!u)
    return false;

  if((pam_error = pam_start(APP_NAME, u->pw_name, &conv, &hnd)) != PAM_SUCCESS) {
    dbgerr("Failed to initialize PAM conversation\n");
    return false;
  } else { /* PAM conversation started */
    if((pam_error = pam_authenticate(hnd, 0)) != PAM_SUCCESS) {
      dbgerr2("%s\n", pam_strerror(hnd, pam_error));
    } else if((pam_error = pam_acct_mgmt(hnd, 0)) != PAM_SUCCESS) {
      dbgerr2("%s\n", pam_strerror(hnd, pam_error));
    }

    pam_end(hnd, pam_error);
    return (pam_error == PAM_SUCCESS)? true: false;
  }
}

static int pam_conv(int num_msg, const struct pam_message **msg, struct pam_response **resp, void *appdata_ptr) {
  struct pam_response *res = calloc(num_msg, sizeof(struct pam_response));

  #define CLEAN_RES do { for(size_t i = 0; i < num_msg; i++) free(res[i].resp); free(res); } while(0)

  for(size_t i = 0; i < num_msg; i++) {
    const struct pam_message *m = msg[i];
    struct pam_response *r = &res[i];

    switch(m->msg_style) {
      case PAM_TEXT_INFO:
        printf("PAM: %s\n", m->msg);
        break;
      case PAM_ERROR_MSG:
        dbgerr(m->msg);
        break;
      case PAM_PROMPT_ECHO_ON:
        {
          char buff[PROMPT_BUFF_LEN];

          printf("%s", m->msg);

          if(!fgets(buff, PROMPT_BUFF_LEN, stdin)) {
            dbgerr("Invalid input");
            CLEAN_RES;
            return PAM_CONV_ERR;
          } else {
            size_t len = strlen(buff);
            buff[len-1] = '\0';
            r->resp = malloc(sizeof(char) * len);
            strncpy(r->resp, buff, len);
          }
        }
        break;
      case PAM_PROMPT_ECHO_OFF:
      {
        struct termios termios_initial, termios_modified;
        int fd = fileno(stdout);

        printf("%s", m->msg);

        if(tcgetattr(fd, &termios_initial) < 0) {
          CLEAN_RES;
          return PAM_CONV_ERR;
        } else {
          memcpy(&termios_modified, &termios_initial, sizeof(struct termios));

          termios_modified.c_lflag ^= ECHO;

          if(tcsetattr(fd, TCSADRAIN, &termios_modified) < 0) {
            CLEAN_RES;
            return PAM_CONV_ERR;
          } else { /* All set, let's ask for something */
            char buff[PROMPT_BUFF_LEN];
            size_t len;

            tcdrain(fd);

            if(!fgets(buff, PROMPT_BUFF_LEN, stdin)) {
              tcsetattr(fd, TCSADRAIN, &termios_initial);
              CLEAN_RES;
              return PAM_CONV_ERR;
            }

            len = strlen(buff);
            buff[len-1] = '\0';

            r->resp = malloc(len * sizeof(char));
            strncpy(r->resp, buff, len);

            fputc('\n', stderr);
            tcdrain(fd);
            tcflush(fd, TCIFLUSH);

            if(tcsetattr(fd, TCSADRAIN, &termios_initial) < 0) {
              dbgerrno;
              CLEAN_RES;
              return PAM_CONV_ERR;
            }
          }
        }
      }
    }
  }

  #undef CLEAN_RES

  *resp = res;
  return PAM_SUCCESS;
}

bool is_user_in_group(const char *user, const char *group) {
  struct passwd *p = getpwnam(user);
  struct group *g;
  bool numeric = true;

  for(size_t i = 0; group[i]; i++)
    if(!isdigit(group[i])) {
      numeric = false;
      break;
    }

  if(numeric) { /* GID given */
    gid_t cgid = (gid_t)strtoul(group, NULL, 10);
    g = getgrgid(cgid);
  } else { /* Group name given */
    g = getgrnam(group);
  }

  if(p->pw_gid == g->gr_gid)
    return true;

  if(g)
    for(char **u = g->gr_mem; *u; u++) {
      if(strcmp(*u, user) == 0)
        return true;
    }

  return false;
}

static char *get_persistance_file_path() {
  struct passwd *p = getpwuid(getuid());
  char *tmpdir = get_temp_dir();
  size_t len = strlen(tmpdir) + 3 /* one '/' and two '.' */ + strlen(APP_NAME) + strlen(BUILD_UUID) + strlen(p->pw_name);
  char *pathname = malloc(sizeof(char) * (len + 1));

  sprintf(pathname, "%s/%s.%s.%s", tmpdir, APP_NAME, BUILD_UUID, p->pw_name);

  free(tmpdir);
  return pathname;
}

void login_set_persistent() {
  char *pathname = get_persistance_file_path();
  int fd = open(pathname, O_WRONLY | O_CREAT, 0400);
  struct timespec times[2];

  dbgnotice2("Updating persistence file [%s]...\n", pathname);
  free(pathname);

  memset(&times, 0, sizeof(times));

  if((times[1].tv_sec = time(&times[0].tv_sec)) == (time_t)-1) {
    dbgerrno;
    dbgerr("Unable to get time\n");
  }

  if(futimens(fd, times) != 0) {
    dbgerrno;
    dbgerr("Unable to set login persistence\n");
  }

  close(fd);
}

bool login_persists() {
  struct stat s;
  char *pathname = get_persistance_file_path();
  bool retval = true;

  dbgnotice2("Checking login persistance [%s]... ", pathname);

  if(stat(pathname, &s) == 0) { /* OK */
    if(!(s.st_mode & S_IRUSR)
      || (s.st_mode & S_IWGRP)
      || (s.st_mode & S_IWOTH)
      || (s.st_uid != 0)
      || (s.st_gid != 0)
    ) {
      retval = false;
    } else { /* Check timer */
      if(s.st_size != 0) {
        dbgerr("\nFile not empty\n");
        retval = false;
      }

      if((time(NULL) - s.st_mtim.tv_sec) >= PERSIST_INTERVAL)
        retval = false;
    }
  } else { /* Stat returned an error */
    if(errno != ENOENT)
      dbgerrno;

    retval = false;
  }

  free(pathname);

  #ifndef NDEBUG
    fprintf(stderr, "%s\n", retval? "TRUE\n": "FALSE\n");
  #endif

  return retval;

  #undef CLEAN
}
